# Initialize services

### Start Ubuntu 20.04 and install required software

```bash
$ apt update
$ apt install docker docker-compose python3 python3-pip python3-venv libffi-dev
```

# Initialize services using docker

We provide a script to initialize the necessary services to run the experiment.

### Clone the repository

```bash
$ git clone https://gitlab.com/hdash/dynamic-situation-testing
```

### Create the virtual environment and install dependencies

```bash
$ python3 -m venv .env
$ source .env/bin/activate
$ pip install -U pip
$ pip install -r requirements.txt
```

### Go to docker directory

```bash 
$ cd docker
```

### Initialize services

```bash
$ docker-compose up
```

### Start tests

```bash
$  python src/main.py -s situations/japan.drl -o results -i data/simulation/patient_1.csv
```
