#!/bin/bash
tendermint testnet --hostname-prefix tendermint- --node-dir-prefix instance- --o /data/tendermint/

for i in 0 1 2 3; do
    sed -i -e 's/create_empty_blocks = true/create_empty_blocks = false/g' \
           -e 's/send_rate = 5120000/send_rate = 102400000/g' \
           -e 's/recv_rate = 5120000/recv_rate = 102400000/g' \
           -e 's/recheck = true/recheck = false/g' \
    /data/tendermint/instance-$i/config/config.toml
done