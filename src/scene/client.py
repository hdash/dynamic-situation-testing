import requests


class SceneClient():
    """
    Client to interact with the scene.
    """

    def __init__(self, api_base):
        self.api_base = api_base

    def insert_entity(self, entity):
        """
        Inserts an entity into the scene.
        """
        r = requests.post('%s/entities' % (self.api_base, ), json=entity)
        return r.json()

    def insert_context(self, entity_id, context):
        """
        Inserts a context to an entity.
        """
        r = requests.post('%s/entities/%s/contexts' %
                          (self.api_base, entity_id), json=context)
        return r.json()

    def insert_context_value(self, entity_id, context_id, value):
        """
        Inserts value to a context.
        """
        r = requests.post('%s/entities/%s/contexts/%s/values' %
                          (self.api_base, entity_id, context_id), json=value)
        return r.json()

    def insert_bulk_context_values(self, entity_id, context_id, values):
        """
        Inserts value to a context.
        """
        r = requests.post('%s/entities/%s/contexts/%s/values/bulk' %
                          (self.api_base, entity_id, context_id), json=values)
        return r.json()

    def load_situations(self, situations):
        """
        Loads situations into the Scene.
        """
        r = requests.post('%s/situations/upload' %
                          (self.api_base, ), json={'situations': situations})
        return r.json()
