from datetime import datetime
import csv
import json
import dateutil.parser
from scene.client import SceneClient


class SceneTesting():

    client: SceneClient

    def __init__(self, scene_api_url) -> None:
        self.client = SceneClient(scene_api_url)

    def load_situations(self, situations) -> bool:
        # load situations into scene
        print(situations)
        situations = self.client.load_situations(situations)
        print(situations)
        if not situations["success"]:
            raise Exception(situations["error"])
        return True

    def get_patient_entity(self, patient_id: str) -> dict:
        return {
            "kind": "Person",
            "descriptor": "patient_%s" % patient_id,
            "attributes": {
                "name": "Patient %s" % patient_id,
                "email": "patient_%s@hdash.com.br" % patient_id,
                "role": "Patient"
            }
        }

    def get_blood_pressure_context(self):
        return {
            "type": "intrinsic",
            "kind": "BloodPressure"
        }

    def get_blood_pressure_value(self, timestamp, systolic, diastolic):
        return {
            "timestamp": timestamp,
            "entries":
                {
                    "systolic": systolic,
                    "diastolic": diastolic
                }
        }

    def init_patients(self, patients):
        entities_ids = {key: {"entity": None, "context": None}
                        for key in set([e for e in patients])}
        for id in entities_ids:
            patient_entity = self.get_patient_entity(id)
            # insert entity
            entity = self.client.insert_entity(patient_entity)
            entities_ids[id]["entity"] = entity["id"]
            # insert blood pressure context
            bp_context = self.get_blood_pressure_context()
            context = self.client.insert_context(entity["id"], bp_context)
            entities_ids[id]["context"] = context["id"]
        return entities_ids

    def insert_blood_pressure_record(self, entity_id, context_id, timestamp, systolic, diastolic):
        context_value = self.get_blood_pressure_value(
            timestamp, systolic, diastolic)
        #print("context_value: {}".format(context_value))
        return self.client.insert_context_value(entity_id, context_id, context_value)

    def load_patient_data(self, entities_ids, csv_path: str) -> bool:
        # load csv file
        input_file = list(csv.DictReader(open(csv_path)))

        # insert blood pressure values
        for line, row in enumerate(input_file):
            print("Inserting record {} from {}".format(line, len(input_file)))
            value = self.get_blood_pressure_value(datetime.timestamp(
                dateutil.parser.isoparse(row["date"])), row["systolic"], row["diastolic"])
            print(entities_ids[row["patient"]]["entity"], entities_ids[row["patient"]]["context"], value)
            self.client.insert_context_value(
                entities_ids[row["patient"]]["entity"], entities_ids[row["patient"]]["context"], value)
