import argparse
from datetime import datetime
import json
import os
import sys
import math
from itertools import repeat

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib
from dateutil.relativedelta import relativedelta


def create_parser():
    parser = argparse.ArgumentParser(
        description='Analysis Tool.')

    parser.add_argument('-r', '--result-file', action='append', type=argparse.FileType('r'))

    parser.add_argument('-l', '--log-level',
                        default='INFO')

    parser.add_argument('-s', '--situation', action='append')
    parser.add_argument('-sw', '--situation-show', action="append")

    parser.add_argument('-lb', '--label',
                        action='append')

    parser.add_argument('-lc', '--label-color',
                        action='append')

    return parser


parser = create_parser()
args = parser.parse_args()

S_ACCEPT = []
M_ACCEPT = []

ts_min = []
ts_max = []

#f, p = plt.subplots()
f, p = plt.subplots(3, 1)

f.set_figwidth(6)
f.set_figheight(12)

longest = 0

_, filename = os.path.split(sys.argv[1])


def to_seconds(s):
    return pd.to_datetime(np.ceil(s / 1000), unit='s')

x_axis = np.array([])
y_axis = np.array([])
for index, json_file in enumerate(args.result_file):
    plt.sca(p[index])
    #with open(json_file) as f:
    data = json.load(json_file)

    vals = {
        "detected_situations": len([d for d in data if d["type"] == "ACTIVATION"]),
        "per_patient": int(len([d for d in data if d["type"] == "ACTIVATION"]) / len(np.unique(np.array([d["situation"]["participations"]["entity"]["id"] for d in data if d["type"] == "ACTIVATION"]))))
    }

    #for index, situation in enumerate(args.situation):
    situation = args.situation[index]
    situation_show = args.situation_show[index] == "1"

    label = args.label[index]
    color = args.label_color[index]
    filtered = [d for d in data if d["type"] == "ACTIVATION" and d["situation"]["type"] == situation]
    if filtered:
        min_ts = min([d["situation"]["participations"]["entity"]["contexts"]["BloodPressure"]["value"]["timestamp"] for d in filtered])
        x = np.array([((datetime.fromtimestamp(d["situation"]["participations"]["entity"]["contexts"]["BloodPressure"]["value"]["timestamp"]))) for d in filtered])
        x_axis = np.concatenate((x, x_axis))
        y = np.array([d["situation"]["participations"]["entity"]["id"] for d in filtered])
        y_axis = np.concatenate((y, y_axis))
        p[index].scatter(x, y, s=100, label=label, marker="s", alpha=0.5, color = color)   

        y_axis = np.concatenate((np.array([11]), y_axis))
        p[index].set_xlim([x_axis.min()- relativedelta(months=3), x_axis.max()+ relativedelta(months=3)])
        p[index].axes.grid(axis="y", color="lightgray")

        _, i = np.unique(y_axis, return_index=True)
        y_axis = y_axis[np.sort(i)]
        plt.yticks(y_axis)
        plt.xticks(rotation=15)

        p[index].legend()
        p[index].set_xlabel('Date')
        p[index].set_ylabel('Patient')

        p[index].set_axisbelow(True)

        #p[0][1].plot(x, [median] * len(x), '--k')

        bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
        p[index].text(0, 100,
                'Activated situations: {detected_situations}\nPer patient: {per_patient}'.format(**vals),
                ha="left", va="bottom", size=15,
                bbox=bbox_props)

#plt.subplots_adjust(left=0.04, right=0.05, bottom=0.04, top=0.05, wspace=0.15, hspace=0.01)
plt.show()
#plt.savefig('results.pdf')  
