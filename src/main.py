#!/usr/bin/env python

import argparse
import asyncio
from datetime import datetime
import io
from multiprocessing import Pool
import os
import time
import typing 
import tqdm
import websockets
import requests
import json
from scene.test import SceneTesting
from urllib.parse import urljoin
import logging
import pandas as pd
import dateutil



class TestStep():
    """
    A step in the test plan
    """
    data: pd.DataFrame
    output_dir: str

    scene_ws_url: str
    scene_ws_situations: str

    def __init__(self, data: pd.DataFrame, output_dir: str, scene_ws_url: str, scene_api_url: str, scene_situations: typing.List[io.TextIOWrapper]):
        self.data = data

        self.output_dir = output_dir
        self.scene_ws_url = scene_ws_url
        self.scene_api_url = scene_api_url
        self.scene_situations = scene_situations
        self.scene_ws_situations = urljoin(self.scene_ws_url, "/ws")

    async def setup(self):
        """
        Setup the test step
        """
        pass

    async def execute(self):
        """
        Execute the test step
        """
        pass

    async def save_results(self, results, file_name, format):
        '''
        save step results
        '''
        scene_results_file = os.path.join(
            self.output_dir, "%s.%s" % (file_name, format))
        rpd = pd.DataFrame.from_dict(results)
        if (format == 'csv'):
            rpd.to_csv(scene_results_file, index=False)
        elif (format == 'json'):
            rpd.to_json(scene_results_file, 'records')


class ListenSceneTestStep(TestStep):

    async def setup(self):
        # init scene
        self.scene_testing = SceneTesting(self.scene_api_url)

        # load scene situations
        self.scene_testing.load_situations(self.scene_situations)

    async def execute(self):
        '''
        listen scene to save results
        '''
        results = []
        running = True
        async with websockets.connect(self.scene_ws_situations, ping_interval=None) as scene_ws:
            while running:
                logging.debug("IS_RUNNING: %s" % (running, ))
                txt_message = await scene_ws.recv()
                logging.info("txt_message %s" % (txt_message,))
                message = json.loads(txt_message)
                if "stop" in message:
                    running = False
                if "type" in message:
                    results.append(message)
                logging.debug("message %s" % (message,))
        await self.save_results(results, "scene_results", "json")




class LoadDataTestStep(TestStep):

    async def setup(self):
        self.scene_testing = SceneTesting(self.scene_api_url)

        # load patients as entites into scene
        self.scene_entities = self.scene_testing.init_patients(
            list(set([e.patient for e in self.data.itertuples()]))[0:10])

    async def notify_scene(self):
        async with websockets.connect(self.scene_ws_situations, ping_interval=None) as websocket:
            await websocket.send(json.dumps({"stop": True}))

    def load_patient_records(self, patient_id, records):
        for record in tqdm.tqdm(records):
            entity_id = self.scene_entities[patient_id]["entity"]
            context_id = self.scene_entities[patient_id]["context"]
            timestamp = datetime.timestamp(dateutil.parser.isoparse(record["date"]))
            response = self.scene_testing.insert_blood_pressure_record(entity_id, context_id, timestamp, str(record["systolic"]), str(record["diastolic"]))
            time.sleep(0.2)

    def load_patient_records_star(self, args):
        self.load_patient_records(*args)

    async def execute(self):
        coroutines = []
        results = []
        # create a pool for each patient
        for patient_id in self.scene_entities:
            patient_records = self.data.loc[self.data['patient']
                                               == patient_id]
            coroutines.append(
                (patient_id, patient_records.to_dict('records')))
        pool = Pool(processes=len(self.scene_entities))
        for _ in tqdm.tqdm(pool.imap_unordered(self.load_patient_records_star, coroutines), total=len(coroutines)):
            #results.extend(data)
            pass
        await self.notify_scene()
        return results

class TestPlan():

    step_classes = [
        ListenSceneTestStep,
        LoadDataTestStep
    ]

    def __init__(self, input_file: str, output_dir: str, scene_ws_url: str, scene_api_url: str, situations: typing.List[io.TextIOWrapper]) -> None:
        self.data: pd.DataFrame = pd.read_csv(input_file)
        self.output_dir = output_dir
        self.scene_ws_url = scene_ws_url
        self.scene_api_url = scene_api_url
        self.situations = situations
        self.steps = self.get_steps()

    def get_step_instance(self, step_class):
        return step_class(self.data, self.output_dir, self.scene_ws_url, self.scene_api_url, self.situations)

    def get_steps(self):
        steps = []
        for step_class in self.step_classes:
            steps.append(self.get_step_instance(step_class))
        return steps

    async def setup(self):
        # prepare de cada teste
        for step in self.steps:
            await step.setup()

    async def execute(self):
        from multiprocessing.pool import ThreadPool
        pool = ThreadPool(processes=len(self.steps))
        results = []

        for step in self.steps:
            results.append(pool.apply_async(asyncio.run, (step.execute(), )))

        pool.close()
        pool.join()
        results = [r.get() for r in results]


def configure_logger(level: str):
    logging.basicConfig(level=level.upper(),
                        format='%(asctime)s %(levelname)s %(message)s')


def create_parser():
    parser = argparse.ArgumentParser(
        description='Test tool.')

    parser.add_argument('-i', '--input-file',
                        type=str)

    parser.add_argument('-o', '--output-dir',
                        type=str)

    parser.add_argument('-l', '--log-level',
                        default='INFO')

    parser.add_argument('-s', '--situation', action='append', type=argparse.FileType('r'))

    parser.add_argument('-sws', '--scene-ws',
                        default='ws://localhost:8080')

    parser.add_argument('-sapi', '--scene-api',
                        default='http://localhost:8080')

    return parser


async def main():
    parser = create_parser()
    args = parser.parse_args()

    configure_logger(args.log_level)

    input_file = args.input_file
    output_dir = args.output_dir
    situations = [sit.read() for sit in args.situation]

    # create out dir
    datestring = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
    output_dir = os.path.join(output_dir, "test_%s" % (datestring))
    os.makedirs(output_dir)

    scene_ws_url = args.scene_ws
    scene_api_url = args.scene_api

    plan = TestPlan(input_file, output_dir, scene_ws_url, scene_api_url, situations)
    await plan.setup()
    await plan.execute()

asyncio.run(main())
