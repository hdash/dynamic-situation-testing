package scene;

import br.com.ufes.lprm.scene.broker.models.context.*;

import br.ufes.inf.lprm.scene.model.Situation;
import br.ufes.inf.lprm.situation.bindings.*;
import br.ufes.inf.lprm.scene.util.SituationHelper;
import java.time.LocalDateTime;
import java.time.Instant;
import java.time.ZoneOffset;

/**
Intermediate event to count ocurrences
**/
declare Event
    kind : String
    timestamp: Long
    context: IntrinsicContext
    systolic: Double
    diastolic: Double
end

/**
High blood pressure situations
**/
declare HighPressureSituation extends Situation
    entity: Entity @part @key
end

/**
Check if the context value is in a morning time
**/
query isMorning (IntrinsicContext icontext)
    Integer(this<12) from LocalDateTime.ofInstant(Instant.ofEpochMilli((long) icontext.getValue().getTimestamp() * 1000), ZoneOffset.UTC).getHour()
end

/**
Check if the context value is in a night time
**/
query isNight(IntrinsicContext icontext)
    Integer(this>18) from LocalDateTime.ofInstant(Instant.ofEpochMilli((long) icontext.getValue().getTimestamp() * 1000), ZoneOffset.UTC).getHour()
end

query accumulateBy(String event, int window, IntrinsicContext icontext)
    Number (this<2) from accumulate (
        Event(kind == event, timestamp>icontext.getValue().getTimestamp()-window,
            timestamp!=icontext.getValue().getTimestamp(),
            this.context.bearer.id == icontext.bearer.id),
        sum(1)
    )
end

/**
Filter blood pressure context
**/
query getBPContext(Entity person, IntrinsicContext icontext)
    icontext := IntrinsicContext(kind == "BloodPressure", value!.entries["systolic"].asNumber > 0, value!.entries["diastolic"].asNumber > 0, person.id == bearer.id)
end

/**
Check accumulated values for a given event in a time window (in miliseconds).
**/
query checkAccumulateMoreThan(String event, int occurrences, int window, IntrinsicContext icontext)
    Number (this>=occurrences) from accumulate(
        Event(kind == event, 
            timestamp>(icontext.getValue().getTimestamp()-(window+(icontext.getValue().getTimestamp()%86400))),
            context.bearer.id == icontext.bearer.id),
        sum(1)
    )
end

query checkAccumulateEqualThan(String event, int occurrences, int window, IntrinsicContext icontext)
    Number (this==occurrences) from accumulate(
        Event(kind == event, 
            timestamp>icontext.getValue().getTimestamp()-(window),
            context.bearer.id == icontext.bearer.id),
        sum(1)
    )
end

/**
Returns the average of the diastolic pressure over the window of time.
**/
query getSystolicAverageFromDay(Double maxSystolic, String event, int window, IntrinsicContext icontext, Double systolicAverage)
    systolicAverage := Number(this>=maxSystolic) from accumulate(
        Event(kind == event,
        timestamp>(icontext.getValue().getTimestamp()-(window+(icontext.getValue().getTimestamp()%86400))),
        context.bearer.id == icontext.bearer.id,
        systolic:systolic),
        average(systolic)
    )
end

query getSystolicAverage(Double maxSystolic, String event, int window, IntrinsicContext icontext, Double systolicAverage)
    systolicAverage := Number(this>=maxSystolic) from accumulate(
        Event(kind == event,
        context.bearer.id == icontext.bearer.id, 
        timestamp>icontext.getValue().getTimestamp()-(window), 
        systolic:systolic),
        average(systolic)
    )
end

/**
Returns the average of the systolic pressure over the window of time
**/
query getDiastolicAverageFromDay(Double maxDiastolic, String event, int window, IntrinsicContext icontext, Double diastolicAverage)
    diastolicAverage := Number(this>=maxDiastolic) from accumulate(
        Event(kind == event, 
        timestamp>(icontext.getValue().getTimestamp()-(window+(icontext.getValue().getTimestamp()%86400))),
        context.bearer.id == icontext.bearer.id,
        diastolic:diastolic),
        average(diastolic)
    )
end

query getDiastolicAverage(Double maxDiastolic, String event, int window, IntrinsicContext icontext, Double diastolicAverage)
    diastolicAverage := Number(this>=maxDiastolic) from accumulate(
        Event(kind == event, 
        context.bearer.id == icontext.bearer.id, 
        timestamp>icontext.getValue().getTimestamp()-(window), 
        diastolic:diastolic),
        average(diastolic)
    )
end


rule "morning"
    when
        $person: Entity(kind == "Person")
        getBPContext($person, $icontext;)
        isMorning($icontext;)
        accumulateBy("HBPMorning", 43200, $icontext;)
    then
        System.out.println("Event occurred at morning");
        Event e = new Event("HBPMorning", $icontext.getValue().getTimestamp(), 
            $icontext, $icontext.getValue().getEntries().get("systolic").asNumber(), 
            $icontext.getValue().getEntries().get("diastolic").asNumber()
        );
        insert(e);
end


rule "night"
    when
        $person: Entity(kind == "Person")
        getBPContext($person, $icontext;)
        isNight($icontext;)
        accumulateBy("HBPNight", 43200, $icontext;)
    then
        System.out.println("Event occurred at night");
        Event e = new Event("HBPNight", $icontext.getValue().getTimestamp(), 
            $icontext, $icontext.getValue().getEntries().get("systolic").asNumber(), 
            $icontext.getValue().getEntries().get("diastolic").asNumber()
        );
        insert(e);
end


rule "twoMeasures"
    when
        $icontext: IntrinsicContext(kind == "BloodPressure", value != null)
        isNight($icontext;)

        checkAccumulateEqualThan("HBPMorning", 2, 86400, $icontext;)
        checkAccumulateEqualThan("HBPNight", 2, 21600, $icontext;)
        checkAccumulateEqualThan("DayMeasure2", 0, 43200, $icontext;)

        getSystolicAverage(0.0, "HBPMorning", 86400, $icontext, $systolicMorningAverage;)
        getSystolicAverage(0.0, "HBPNight", 21600, $icontext, $systolicNightAverage;)

        getDiastolicAverage(0.0, "HBPMorning", 86400, $icontext, $diastolicMorningAverage;)
        getDiastolicAverage(0.0, "HBPNight", 21600, $icontext, $diastolicNightAverage;)
    then
        System.out.println("DayMeasure2");
        System.out.println(($systolicMorningAverage+$systolicNightAverage)/2);
        System.out.println(($diastolicMorningAverage+$diastolicNightAverage)/2);
        Event e = new Event("DayMeasure2", $icontext.getValue().getTimestamp(), $icontext, ($systolicMorningAverage+$systolicNightAverage)/2, ($diastolicMorningAverage+$diastolicNightAverage)/2);
        insert(e);
end

rule "dispareMessagePressure"
@role(situation)
@type(HighPressureSituation)
	when
        //$person: Entity(kind == "Person")
        $icontext: IntrinsicContext(kind == "BloodPressure", value != null, entity:bearer)
        checkAccumulateMoreThan("DayMeasure2", 5, 432000, $icontext;)
        getSystolicAverageFromDay(135.0, "DayMeasure2", 432000, $icontext, $systolicAverage;)
        getDiastolicAverageFromDay(85.0, "DayMeasure2", 432000, $icontext, $diastolicAverage;)
	then
        System.out.println("Situação detectada");
        System.out.println($systolicAverage);
        System.out.println($diastolicAverage);
        SituationHelper.situationDetected(drools);
end